#! /usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'sotona'
"""
Генерирует SVG файл с изображением:
 - плоского ломаного бруса;
 - связями (шарнир на опоре, заделка, стержень с шарнирами и т.д.);
 - приложенными к брусу силами, сосредоточенными и\или распределёнными;

 - аннотацией, с указанием велечин сил и длинн секций бруса, а также их углами по отношению к горизонтальной плоскости,
   (положитльный отсчёт угла - против часовой стрелки)

Форма и длинна бруса генерируется случайным образом.

TODO:
* типы связей и силы генерируются случайным образом.
* задача статически разрешимая
+ силы (распределённые и сосредоточенные), угл под которой дейсвует сила
+ печатать решение, отдельно от файла
+ углы для балки
+ моменты



   Сгенерированые схемы должны быть
"""
from random import randint, random, choice
from PIL import Image, ImageDraw
import cairo
from math import *

links =            ['шарнир на неподвижной опоре', 'заделка', 'стержень', 'шарнир на подвижной опоре']
links_reaktions =  [2                            , 3        , 1         , 1]


def get_beams_slope(beam):
    """
    beam = [(x,y), (x,y)]
    return -- angle of slope [rad]"""
    x,y = beam[0]
    x1,y1 = beam[1]
    return atan((y1-y+0.0001)/(x1-x+0.0001))


def gen_beam():
    """return list of (x,y)"""
    beam = [(0,0)]
    max_len = 5
    for i in range(0,3):
        angle =  1.0*choice([0,30,45,60,90])/90*pi/2
        l = randint(1,5)
        x0,y0 = beam[-1][0], beam[-1][1]
        beam.append((l*cos(angle)+x0, l*sin(angle)+y0))
    print(beam)
    return beam


def note(beam):
    abc = "ABCD"
    l = []
    note = ""
    for i in range(1,len(beam)):
        p = beam[i-1]
        p2 = beam[i]
        l.append(sqrt((p2[0]-p[0])**2 + (p2[1]-p[1])**2))
        note += "%s = %1.0f м%s"%(abc[i-1]+abc[i],l[i-1], ", " if i!=len(beam)-1 else "")
        i+=1
    return note


def draw_surface(ctx, x1, x2, y, ds):
    """
    ctx - cairo context
    x1, x2 - from and to
    ds - size to determine slash length and etc """
    ctx.move_to(x1, y)
    ctx.line_to(x2, y)
    dslash = ds/4
    n = int(ceil((x2-x1)/dslash))
    for i in range(1,n+1):
        ctx.move_to(-dslash/2 + dslash*i + x1,        y)
        ctx.line_to(-dslash/2 + dslash*i + x1 - ds/4, y + ds/4)
    ctx.close_path()
    ctx.stroke()


def draw_fixed_hinge(ctx, beam, ds, width):
    """
    ctx - cairo cotext
    beam = [(x,y),(x,y)]- two points of beam
    ds - small size. needed to determine triangle size and etc
    width - wisth of line """
    x,y = beam[0]
    x1,y1 = beam[1]
    mx = 1.6
    alpha = get_beams_slope(beam)
    dx, dy = ds/2, ds/1.6

    ctx.set_source_rgb(0,0,0) # выбираем цвет
    ctx.set_line_width(width)
    # Шарнир на опоре
    ctx.save()
    ctx.translate(x,y)
    ctx.rotate(alpha  + pi/2)
    x,y=0,0
    ctx.move_to(x,y)
    ctx.arc(x, y, ds/8, 0, 2 * pi)
    ctx.stroke()
    ctx.move_to(x,y)
    # triangle with _ _
    ctx.line_to(x-dx, y+dy)
    ctx.line_to(x-dx*mx, y+dy)
    ctx.line_to(x+dx*mx, y+dy)
    ctx.line_to(x+dx, y+dy)
    ctx.line_to(x, y)

    draw_surface(ctx, x-dx*mx, x+dx*mx, y+dy, ds)

    ctx.close_path()
    ctx.stroke()
    ctx.restore()


def draw_rolled_hinge(ctx, beam, alpha, ds, width):
    """
    ctx - cairo cotext
    beam = [(x,y),(x,y)]- two points of beam
    ds - small size. needed to determine triangle size and etc
    width - wisth of line

    hinge drawed near first beam node
    """
    r = ds/8
    x,y = beam[0]
    x1,y1 = beam[1]
    mx = 1.6
    # alpha = get_beams_slope(beam)
    dx, dy = ds/2, ds/1.6

    ctx.set_source_rgb(0,0,0) # выбираем цвет
    ctx.set_line_width(width)
    # Шарнир на опоре
    ctx.save()
    ctx.translate(x,y)
    ctx.rotate(alpha  - pi/2)
    x,y=0,0
    ctx.move_to(x,y)
    ctx.arc(x, y, r, 0, 2 * pi)
    ctx.stroke()
    ctx.move_to(x,y)
    # triangle with _ _
    ctx.line_to(x-dx, y+dy)
    ctx.line_to(x+dx, y+dy)
    ctx.line_to(x, y)
    ctx.close_path()
    ctx.stroke()
    y=dy+r
    ctx.arc(x-r*2.1, y, r, 0, 2 * pi)
    ctx.stroke()
    ctx.arc(x+r*2.1, y, r, 0, 2 * pi)
    draw_surface(ctx, x-dx*mx, x+dx*mx, y+r, ds)
    ctx.close_path()
    ctx.stroke()
    ctx.restore()


def draw_rod_with_hinges(ctx, xy, alpha, ds, width):
    """
    xy = (x,y)
    alpha - angle of the rod relative to Ox [rad]
    ds - size to determine rod length
    width - width of the line  """
    jr = ds/8  #  joint radius
    x,y=xy
    l = ds  # rod length
    x1,y1 = x+l*cos(alpha), y+l*sin(alpha)

    ctx.set_source_rgb(0,0,0)
    ctx.set_line_width(width)
    # draw rod with hinges
    ctx.arc(x, y, ds/8, 0, 2 * pi)
    ctx.move_to(x+jr*0.8, y+jr*0.8)
    ctx.line_to(x1-jr*0.8, y1-jr*0.8)
    ctx.move_to(x1+jr, y1)
    ctx.arc(x1, y1, jr, 0, 2 * pi)
    ctx.close_path()
    ctx.stroke()
    ctx.save()
    ctx.translate(x1,y1)
    ctx.rotate(alpha  - pi/2)
    x,y=0,0
    draw_surface(ctx,x-ds/1.5, x+ds/1.5, y, ds)
    ctx.restore()


def draw_fixed_support(ctx, beam, ds, width):
    # beam - [(x,y), (x1,y1)]
    x,y = beam[-1]
    alpha = get_beams_slope(beam)

    ctx.save()
    ctx.translate(x,y)
    x = y = 0
    ctx.rotate(alpha-pi/2)
    ctx.move_to(x,y)
    draw_surface(ctx, x-ds, x+ds, y, ds)
    ctx.restore()
    pass


def draw_force(ctx, xy, F, alpha,  ds, width):
    """
    ctx - cairo context;
    xy - point of application of the force;
    alpha - angle of force relative to Ox, positive - counterclockwise;
    F - length of the vector;
    ds - small size,
    width - line's width
    """
    # alpha += pi/2
    x,y = xy
    x1,y1 = x+F*cos(alpha), y-F*sin(alpha)
    ctx.set_line_width(width)
    ctx.move_to(x1,y1)
    ctx.line_to(x,y)
    ad = pi/8
    al = ds/2
    angle = atan2 (y1 - y, x1 - x)
    # left and right coordinates of the arrow
    xl = x + al * cos(angle - ad);
    yl = y + al * sin(angle - ad);
    xr = x + al * cos(angle + ad);
    yr = y + al * sin(angle + ad);
    ctx.move_to(x,y)
    ctx.line_to(xl,yl)
    ctx.move_to(x,y)
    ctx.line_to(xr,yr)
    ctx.close_path()
    ctx.stroke()

def draw_beam(beam, note):
    size = 256
    note_size = 20
    nsize = 15
    l_width = 2
    t_size = 8
    ds = size/nsize
    x0 = y0 = 2*ds

    beam_g = []  # Beam with graphical coordinates
    for p in beam:
        beam_g.append((p[0]*ds+x0, p[1]*ds+y0))

    svg=cairo.SVGSurface("s1.svg",size, size+note_size)
    ctx=cairo.Context(svg)

    # Сетка
    ctx.set_source_rgb(.5,.5,.5) # выбираем цвет
    ctx.set_line_width(l_width/4) # выбираем линию
    # ctx.set_line_width(1) # выбираем линию
    for i in range(1,nsize):
        ctx.move_to(i*ds,0) # перемещаем «курсор»
        ctx.line_to(i*ds,size) # рисуем линию от позиции «курсора» до x2,y2
        ctx.move_to(0,i*ds) # перемещаем «курсор»
        ctx.line_to(size, i*ds) # рисуем линию от позиции «курсора» до x2,y2
    ctx.close_path() # замыкаем контур
    ctx.stroke() # обводим контур выбранной линией

    # Брус
    ctx.set_line_width(l_width)
    ctx.set_source_rgb(0,0,0)
    ctx.move_to(beam[0][0],beam[0][1])
    for i in range(0,len(beam_g)-1):
        p = beam_g[i]
        ctx.move_to(p[0], p[1])
        p = beam_g[i+1]
        ctx.line_to(p[0], p[1])
    ctx.close_path() # замыкаем контур
    ctx.stroke() # обводим контур выбранной линией

    # Точки
    abc =  "ABCDE"
    ctx.set_source_rgb(.2,.2,.2)
    ctx.set_font_size(t_size)
    for i in range(0,len(beam_g)):
        p = beam_g[i]
        ctx.move_to(p[0], p[1])
        ctx.show_text(abc[i])

    # Связи
    draw_fixed_hinge(ctx, beam_g[0:2], ds, l_width/2)
    # draw_rod_with_hinges(ctx, beam_g[-1], pi/3, ds, l_width/2)
    # draw_fixed_support(ctx, beam_g[-2:], ds, l_width)
    draw_rolled_hinge(ctx, beam_g[-2:][::-1], pi/3, ds, l_width/2)

    # Силы и моменты
    ctx.set_source_rgb(0,0,1)
    draw_force(ctx, beam_g[choice([i for i in range(1,len(beam_g))])], ds*1.8, i * pi/8, ds, l_width/2)

    ctx.set_source_rgb(0,0,0) # выбираем цвет
    ctx.set_font_size(t_size) # выбираем размер шрифта, можно дробный
    ctx.move_to(0,size+8)
    ctx.show_text(note)
    svg.finish()


print("Statics task compositor")
beam = gen_beam()
note_ = note(beam)
print(note_)
draw_beam(beam, note_)